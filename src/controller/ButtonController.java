package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JOptionPane;
import model.Game;
import view.FootPanel;
import view.MainForm;

public class ButtonController implements ActionListener, ItemListener {

    private Game game;
    private FootPanel fp;

    public ButtonController(Game game, FootPanel fp) {
        this.game = game;
        this.fp = fp;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Check")) {
            ++game.checkCount;

            if (game.checkCount > 3) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(new MainForm(),
                        "You lost.\nWould you like to continue?",
                        "Message",
                        dialogButton, JOptionPane.QUESTION_MESSAGE);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    //continue game
                    game.newGame('E');
                } else {
                    System.exit(0);
                }
            }
            game.checkGame();
            if (fp.getTimeChek()) {
                game.timer.stop();
                fp.update(game.getTime());
            }
        } else if (e.getActionCommand().equals("Enter your")) {
            game.newEmptyGame();
            fp.setStepCheck(false);
        } else if (e.getActionCommand().equals("Ok")) {
            game.solveGame();
            fp.setStepCheck(true);
            fp.update(game.getStep());
        } else {
            game.setSelectedNumber(Integer.parseInt(e.getActionCommand()));
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            Object item = e.getItem();
            if (item == "Easy") {
                game.newGame('E');
                fp.update(game.getStep());
            } else if (item == "Medium") {
                game.newGame('M');
                fp.update(game.getStep());
            } else if (item == "Hard") {
                game.newGame('H');
                fp.update(game.getStep());
            }
        }
    }
}
