package controller;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;
import model.Game;
import view.Field;
import view.SudokuPanel;
import view.FootPanel;
import view.MainForm;

public class SudokuController implements MouseListener, ActionListener {

    private SudokuPanel sudokuPanel;    // Panel to control.
    private Game game;                  // Current Sudoku game.
    private FootPanel fp;               // Current foot panel.

    public SudokuController(SudokuPanel sudokuPanel, Game game, FootPanel fp) {
        this.sudokuPanel = sudokuPanel;
        this.game = game;
        this.fp = fp;
    }

    public void mousePressed(MouseEvent e) {
        JPanel panel = (JPanel) e.getSource();
        Component component = panel.getComponentAt(e.getPoint());
        if (component instanceof Field) {
            Field field = (Field) component;
            int x = field.getFieldX();
            int y = field.getFieldY();
            if (e.getButton() == MouseEvent.BUTTON1 && game.getNumber(x, y) == 0) {
                //fill the field
                int number = game.getSelectedNumber();
                if (number == -1) {
                    return;
                }
                game.setNumber(x, y, number);
                field.setNumber(number, 2);
                
                if (fp.getStepCheck(true)) {
                    //count steps
                    game.setStep(game.getStep() + 1);
                    fp.update(game.getStep());

                    if (game.getStep() == 1 && fp.getTimeChek()) {
                        //start timer
                        game.timer = new Timer(1000, this);
                        game.timer.setDelay(1000);
                        game.timer.start();
                        
                    } else if (fp.getTimeChek()) {
                        game.timer.start();
                    }
                }

            } else if (e.getButton() == MouseEvent.BUTTON3 && !field.getForeground().equals(Color.BLACK)) {
                //clear the field
                game.setNumber(x, y, 0);
                field.setNumber(0, 4);
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if(!fp.getTimeChek() && game.getStep() == 0){
         game.timer.removeActionListener(this);
         game.resetTimeto0();
        }
        else
        game.setTime(-1);
        fp.update(game.getTime());

        //propose to exit and msg
        if (game.timeIsOver) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(new MainForm(),
                    "You lost.\nWould you like to continue?",
                    "Message",
                    dialogButton, JOptionPane.QUESTION_MESSAGE);
            if (dialogResult == JOptionPane.YES_OPTION) {
                //continue game
                game.newGame('E');
            } else {
                System.exit(0);
            }
        }
    }
}
