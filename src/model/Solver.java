package model;

import java.util.stream.IntStream;

public class Solver {

    int[][] board;

    Solver(int[][] b) {
        board = new int[9][9];
        for (int y = 0; y < 9; y++) {
            System.arraycopy(b[y], 0, board[y], 0, 9);
        }
    }

    public boolean solve(int[][] board) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                if (board[y][x] == 0) {
                    for (int k = 1; k <= 9; k++) {
                        board[y][x] = k;
                        if (isValid(board, y, x) && solve(board)) {
                            return true;
                        }
                        board[y][x] = 0;
                    }
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isValid(int[][] board, int y, int x) {
        return ySolve(board, y)
                && xSolve(board, x)
                && sectionSolve(board, y, x);
    }

    public boolean sectionSolve(int[][] board, int y, int x) {
        boolean[] buf = new boolean[9];
        int yStart = (y / 3) * 3;
        int yEnd = yStart + 3;

        int xStart = (x / 3) * 3;
        int xEnd = xStart + 3;

        for (int r = yStart; r < yEnd; r++) {
            for (int c = xStart; c < xEnd; c++) {
                if (!checkbuf(board, r, buf, c)) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean xSolve(int[][] board, int x) {
        boolean[] buf = new boolean[9];
        return IntStream.range(0, 9)
                .allMatch(y -> checkbuf(board, y, buf, x));
    }

    public boolean ySolve(int[][] board, int y) {
        boolean[] buf = new boolean[9];
        return IntStream.range(0, 9)
                .allMatch(x -> checkbuf(board, y, buf, x));
    }

    public boolean checkbuf(int[][] board, int y, boolean[] buf, int x) {
        if (board[y][x] != 0) {
            if (!buf[board[y][x] - 1]) {
                buf[board[y][x] - 1] = true;
            } else {
                return false;
            }
        }
        return true;
    }
}
