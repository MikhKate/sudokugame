package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Random;
import javax.swing.Timer;

public class Game extends Observable {

    private int[][] solution;       // generated solution.
    private int[][] game;           // generated game with user input.
    private boolean[][] check;      // holder for checking validity of game.
    private int selectedNumber;     // selected number by user.
    private char level;             // game level
    private int step, mm, ss = 60;  //
    private String time = "";       // time, step counter, minutes, seconds 
    public Timer timer;             //
    public boolean timeIsOver = false;//flag to stop timer
    public int checkCount = 0;      //counter for push button 'Check' (max=3)
    private Solver solver;          //clas with solve algorithm

    public Game() {
        this.level = 'E';
        newGame(level);//default easy level   
        check = new boolean[9][9];
        mm = 10;
    }

    public void newGame(char level) {
        check = new boolean[9][9];
        solution = generateSolution(new int[9][9], 0);
        this.level = level;
        game = generateGame(copy(solution));
        setChanged();
        setStep(0);
        resetTime();
        timeIsOver = false;
        checkCount = 0;
        /*  for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                System.out.print(solution[y][x]+" ");
            }
            System.out.println("");
        }*/
        notifyObservers(UpdateAction.NEW_GAME);

    }

    public void newEmptyGame() {
        for (int x = 0; x < 9; x++) {
            for (int y = 0; y < 9; y++) {
                solution[x][y] = 0;
            }
        }
        game = generateGame(copy(solution));
        setChanged();
        setStep(0);
        notifyObservers(UpdateAction.NEW_EMPTY_GAME);
    }

    public void solveGame() {
        check = new boolean[9][9];

        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                solution[y][x] = game[y][x];
            }
        }
        solver = new Solver(solution);
        if (solver.solve(solution)) {
            setChanged();
            setStep(0);
            resetTime();
            timeIsOver = false;
            checkCount = 0;
            notifyObservers(UpdateAction.NEW_GAME);
        } else {
            notifyObservers(UpdateAction.WARNING);
        }
    }

    public void checkGame() {
        selectedNumber = 0;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                check[y][x] = game[y][x] == solution[y][x];
            }
        }
        setChanged();
        notifyObservers(UpdateAction.CHECK);
    }

    public void setSelectedNumber(int selectedNumber) {
        this.selectedNumber = selectedNumber;
        setChanged();
        notifyObservers(UpdateAction.SELECTED_NUMBER);
    }

    public int getSelectedNumber() {
        return selectedNumber;
    }

    public void setNumber(int x, int y, int number) {
        game[y][x] = number;
    }

    public int getNumber(int x, int y) {
        return game[y][x];
    }

    public boolean isCheckValid(int x, int y) {
        return check[y][x];
    }

    private boolean isPossibleX(int[][] game, int y, int number) {
        for (int x = 0; x < 9; x++) {
            if (game[y][x] == number) {
                return false;
            }
        }
        return true;
    }

    private boolean isPossibleY(int[][] game, int x, int number) {
        for (int y = 0; y < 9; y++) {
            if (game[y][x] == number) {
                return false;
            }
        }
        return true;
    }

    private boolean isPossibleBlock(int[][] game, int x, int y, int number) {
        int x1 = x < 3 ? 0 : x < 6 ? 3 : 6;
        int y1 = y < 3 ? 0 : y < 6 ? 3 : 6;
        for (int yy = y1; yy < y1 + 3; yy++) {
            for (int xx = x1; xx < x1 + 3; xx++) {
                if (game[yy][xx] == number) {
                    return false;
                }
            }
        }
        return true;
    }

    private int getNextPossibleNumber(int[][] game, int x, int y, List<Integer> numbers) {
        while (numbers.size() > 0) {
            int number = numbers.remove(0);
            if (isPossibleX(game, y, number) && isPossibleY(game, x, number) && isPossibleBlock(game, x, y, number)) {
                return number;
            }
        }
        return -1;
    }

    private int[][] generateSolution(int[][] game, int index) {
        if (index > 80) {
            return game;
        }

        int x = index % 9;
        int y = index / 9;

        List<Integer> numbers = new ArrayList<Integer>();
        for (int i = 1; i <= 9; i++) {
            numbers.add(i);
        }
        Collections.shuffle(numbers);

        while (numbers.size() > 0) {
            int number = getNextPossibleNumber(game, x, y, numbers);
            if (number == -1) {
                return null;
            }

            game[y][x] = number;
            int[][] tmpGame = generateSolution(game, index + 1);
            if (tmpGame != null) {
                return tmpGame;

            }
            game[y][x] = 0;
        }

        return null;
    }

    private int[][] generateGame(int[][] game) {
        List<Integer> positions = new ArrayList<Integer>();
        for (int i = 0; i < 81; i++) {
            positions.add(i);
        }
        Collections.shuffle(positions);
        return generateGame(game, positions);
    }

    private int[][] generateGame(int[][] game, List<Integer> positions) {
        while (positions.size() > 0) {
            int position = positions.remove(0);
            int x = position % 9;
            int y = position / 9;
            int temp = game[y][x];
            game[y][x] = 0;

            if (!isValid(game)) {
                game[y][x] = temp;
            }
        }
        if (level == 'E') {
            toEasy(game);
            mm = 10;
        } else if (level == 'M') {
            mm = 15;
        } else if (level == 'H') {
            toHard(game);
            mm = 24;
        }
        return game;
    }

    private boolean isValid(int[][] game) {
        return isValid(game, 0, new int[]{0});
    }

    private boolean isValid(int[][] game, int index, int[] numberOfSolutions) {
        if (index > 80) {
            return ++numberOfSolutions[0] == 1;
        }

        int x = index % 9;
        int y = index / 9;

        if (game[y][x] == 0) {
            List<Integer> numbers = new ArrayList<Integer>();
            for (int i = 1; i <= 9; i++) {
                numbers.add(i);
            }

            while (numbers.size() > 0) {
                int number = getNextPossibleNumber(game, x, y, numbers);
                if (number == -1) {
                    break;
                }
                game[y][x] = number;

                if (!isValid(game, index + 1, numberOfSolutions)) {
                    game[y][x] = 0;
                    return false;
                }
                game[y][x] = 0;
            }
        } else if (!isValid(game, index
                + 1, numberOfSolutions)) {
            return false;
        }

        return true;
    }

    //show +5 random field from solution
    private void toEasy(int[][] game) {
        int trys = 0;
        int[][] numb = new int[1][2];
        while (trys < 5) {
            numb = init();
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    if (game[y][x] == 0 && numb[0][0] == y && numb[0][1] == x) {
                        game[y][x] = solution[y][x];
                        ++trys;
                    }
                }
            }
        }
    }

    //hide 4 random field from solution
    private void toHard(int[][] game) {
        int trys = 0;
        int[][] numb = new int[1][2];
        while (trys < 4) {
            numb = init();
            for (int y = 0; y < 9; y++) {
                for (int x = 0; x < 9; x++) {
                    if (game[y][x] != 0 && numb[0][0] == y && numb[0][1] == x) {
                        game[y][x] = 0;
                        ++trys;
                    }
                }
            }
        }
    }

    private int[][] init() {
        Random r = new Random(System.currentTimeMillis());
        int[][] ind = new int[1][2];
        int x, y;
        x = r.nextInt(9);
        y = r.nextInt(9);
        ind[0][1] = x;
        ind[0][0] = y;
        return ind;
    }

    public int getStep() {
        return this.step;
    }

    public void setStep(int s) {
        this.step = s;
    }

    public void setTime(int t) {
        if (timeIsOver == false) {
            ss += t;
            mm = mm;

            if (ss == 0) {
                --mm;
                ss = 59;
            }
            time = mm + ":" + ss;

            if (mm == 0 && ss == 1) {
                ss--;
                time = mm + ":" + ss;
                timeIsOver = true;
                timer.stop();
            }
        }
    }

    public String getTime() {
        return time;
    }

    public void resetTime() {
        ss = 60;
        time = mm + ":" + ss;
    }

    public void resetTimeto0() {
        time = 0 + ":" + 0;
    }

    private int[][] copy(int[][] game) {
        int[][] copy = new int[9][9];
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                copy[y][x] = game[y][x];
            }
        }
        return copy;
    }

}
