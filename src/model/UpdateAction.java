package model;

public enum UpdateAction {
    NEW_GAME,
    CHECK,
    SELECTED_NUMBER,
    NEW_EMPTY_GAME,
    WARNING
}