package view;

import java.awt.Dimension;
import java.awt.Color;
import java.util.Observable;
import java.util.Observer;
import javax.swing.*;

import controller.ButtonController;
import model.UpdateAction;

public class ButtonPanel extends JPanel implements Observer {

    JButton btnNew, btnCheck, btnYourGame, btnOk;   // Used buttons.
    ButtonGroup bgNumbers;          // Group for grouping the toggle buttons.  
    JToggleButton[] btnNumbers;     // Used toggle buttons.
    JComboBox choosenLevel;         //Combo box for choose game level

    public ButtonPanel() {

        GroupLayout layout = new GroupLayout(this);
        super.setLayout(layout);
        super.setBackground(Color.WHITE);
        layout.setAutoCreateGaps(true);
        //layout.setAutoCreateContainerGaps(true);

        //-------------Choose Level--------------
        JLabel jLabel1 = new JLabel("Level:");
        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14));
       
        String[] levelname = {"Easy", "Medium", "Hard"};
        choosenLevel = new JComboBox(levelname);
       // choosenLevel.setModel(new DefaultComboBoxModel<String>());
     
        //-------------Separators--------------
        JSeparator s1 = new JSeparator();      
        JSeparator s2 = new JSeparator();

        //-------------Number buttons--------------
        bgNumbers = new ButtonGroup();
        btnNumbers = new JToggleButton[9];
        for (int i = 0; i < 9; i++) {

            btnNumbers[i] = new JToggleButton("" + (i + 1));
            btnNumbers[i].setPreferredSize(new Dimension(35, 35));
            btnNumbers[i].setFocusable(false);
            btnNumbers[i].setFont(new java.awt.Font("Tahoma", 0, 12));

            bgNumbers.add(btnNumbers[i]);
        }
        //-------------Chek button--------------
        btnCheck = new JButton("Check");
        btnCheck.setFocusable(false);
        btnCheck.setSize(80, 25);
        //-------------"Enter your" button--------------
        btnYourGame = new JButton("Enter your");
        btnYourGame.setFocusable(false);
        btnYourGame.setSize(80, 25);
        btnOk = new JButton("Ok");
        btnOk.setFocusable(false);
        btnOk.setSize(50, 25);


        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addComponent(jLabel1)
                                                .addComponent(choosenLevel, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addComponent(btnNumbers[0])
                                                .addComponent(btnNumbers[1])
                                                .addComponent(btnNumbers[2]))
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addComponent(btnNumbers[3])
                                                .addComponent(btnNumbers[4])
                                                .addComponent(btnNumbers[5]))
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addComponent(btnNumbers[6])
                                                .addComponent(btnNumbers[7])
                                                .addComponent(btnNumbers[8]))
                                        .addComponent(btnCheck, GroupLayout.Alignment.CENTER)
                                        .addGroup(GroupLayout.Alignment.CENTER, layout.createSequentialGroup()
                                                .addComponent(btnYourGame)
                                                .addComponent(btnOk))
                                        .addComponent(s1, GroupLayout.Alignment.CENTER, GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                                        .addComponent(s2, GroupLayout.Alignment.CENTER))
                                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel1)
                                        .addComponent(choosenLevel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addGap(10, 10, 10)
                                .addComponent(s1, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 5, 5)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnNumbers[0], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[1], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[2], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnNumbers[3], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[4], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[5], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnNumbers[6], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[7], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnNumbers[8], GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                                .addComponent(btnCheck)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(s2, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                        .addComponent(btnYourGame)
                                        .addComponent(btnOk))
                                .addContainerGap(46, Short.MAX_VALUE))
        );
    }

    public void update(Observable o, Object arg) {
        switch ((UpdateAction) arg) {
            case NEW_GAME:     
            case CHECK:
                bgNumbers.clearSelection();
                break;
        }
    }

    public void setController(ButtonController buttonController) {

        choosenLevel.addItemListener(buttonController);
        btnCheck.addActionListener(buttonController);
        btnYourGame.addActionListener(buttonController);
        btnOk.addActionListener(buttonController);
        for (int i = 0; i < 9; i++) {
            btnNumbers[i].addActionListener(buttonController);
        }
    }
}
