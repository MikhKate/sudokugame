package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;

public class Field extends JLabel {

    private int x;      // X position in game.
    private int y;      // Y position in game.

    public Field(int x, int y) {
        super("", CENTER);
        this.x = x;
        this.y = y;

        setPreferredSize(new Dimension(40, 40));
        setBorder(BorderFactory.createLineBorder(Color.GRAY));
        setFont(new Font(Font.DIALOG, Font.PLAIN, 20));
        setOpaque(true);
    }

    public void setNumber(int number, int userInput) {
        if (userInput == 1) {
            setForeground(Color.decode("#8157ba"));//6e48a3/7630d9//violet
        } else if (userInput == 2) {
            setForeground(Color.decode("#26b0bf"));//blue
        } else if (userInput == 3) {
            setForeground(Color.BLACK);
        } else {
            setForeground(Color.GRAY);//for 0 fileds
        }
        setText(number > 0 ? number + "" : "");
    }

    public int getFieldX() {
        return x;
    }

    public int getFieldY() {
        return y;
    }
}
