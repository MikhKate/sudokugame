package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import controller.SudokuController;
import java.awt.Graphics;
import javax.swing.JOptionPane;
import model.Game;
import model.UpdateAction;

public class SudokuPanel extends JPanel implements Observer {

    private Color colorBack = Color.WHITE;
    private Field[][] fields;
    private JPanel[][] panels;
    private boolean allRight = false;
    private int blackfields =0;

    @Override
    public void print(Graphics g) {
        super.print(g);
    }

    public SudokuPanel() {
        super(new GridLayout(3, 3));

        panels = new JPanel[3][3];
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                panels[y][x] = new JPanel(new GridLayout(3, 3));
                panels[y][x].setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
                add(panels[y][x]);
            }
        }

        fields = new Field[9][9];
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                fields[y][x] = new Field(x, y);
                panels[y / 3][x / 3].add(fields[y][x]);
            }
        }
    }

    public void update(Observable o, Object arg) {
        switch ((UpdateAction) arg) {
            case NEW_GAME:
                setGame((Game) o);
                break;
            case CHECK:
                allRight = setGameCheck((Game) o);
                if (allRight) {
                    winMessage((Game) o);
                }
                break;
            case SELECTED_NUMBER:
                setNumbC((Game) o);
                break;
            case NEW_EMPTY_GAME:
                setGame((Game) o);
                break;
        }
    }

    public void setGame(Game game) {
        blackfields = 0;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                fields[y][x].setBackground(colorBack);
                if (game.getNumber(x, y) == 0) {
                    fields[y][x].setNumber(game.getNumber(x, y), 4);
                } else if (game.getNumber(x, y) > 0) {
                    fields[y][x].setNumber(game.getNumber(x, y), 3);
                    blackfields++;
                }
            }
        }
    }

    public void setNumbC(Game game) {
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                fields[y][x].setBackground(colorBack);
                if (fields[y][x].getForeground() != Color.BLACK) {
                    fields[y][x].setNumber(game.getNumber(x, y), 1);
                }
            }
        }
    }

    private boolean setGameCheck(Game game) {
        int numbRightFiled = 0;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                fields[y][x].setBackground(colorBack);
                if (!fields[y][x].getForeground().equals(Color.BLACK)) {
                    if (game.getNumber(x, y) != 0) {
                        fields[y][x].setBackground(game.isCheckValid(x, y) ? Color.GREEN : Color.RED);
                        if (fields[y][x].getBackground() == Color.GREEN) {
                            numbRightFiled++;
                        }
                    }
                }
            }
        }
        return (numbRightFiled + blackfields) == 81;
    }

    public void setFieldColor(Color c) {
        colorBack = c;
        for (int y = 0; y < 9; y++) {
            for (int x = 0; x < 9; x++) {
                fields[y][x].setBackground(c);
            }
        }
    }

    public void setController(SudokuController sudokuController) {
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 3; x++) {
                panels[y][x].addMouseListener(sudokuController);
            }
        }
    }

    public void winMessage(Game game) {

        int dialogButton = JOptionPane.YES_NO_OPTION;
        int dialogResult = JOptionPane.showConfirmDialog(new MainForm(),
                "Congratulations!!!\nYou win.\nWould you like to continue?",
                "Message",
                dialogButton, JOptionPane.QUESTION_MESSAGE);
        if (dialogResult == JOptionPane.YES_OPTION) {
            //continue game
            allRight = false;
            game.checkCount = 0;
            game.newGame('E');
        } else {
            System.exit(0);
        }
    }
}
