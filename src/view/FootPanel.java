package view;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class FootPanel extends JPanel {

    private JLabel currTime;
    private JLabel currStep;
    private boolean timeCheked = false;
    private boolean StepCheked = true;

    public FootPanel() {

        GroupLayout layout = new GroupLayout(this);
        super.setLayout(layout);

        currTime = new JLabel("0");
        currStep = new JLabel("0");
        JLabel jLabel2 = new JLabel("Timer:");
        JLabel jLabel3 = new JLabel("Steps:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12));
        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12));

        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addComponent(currTime, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(currStep, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
                                .addGap(5, 10, 21))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                                .addComponent(currTime)
                                .addComponent(jLabel3, GroupLayout.DEFAULT_SIZE, 21, Short.MAX_VALUE)
                                .addComponent(currStep)
                                .addComponent(jLabel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

    }

    public void update(int step) {
        currStep.setText("" + step);
    }

    public void update(String t) {
        currTime.setText(t);
    }

    public void setStepCheck(boolean chek) {
        StepCheked = chek;
    }

    public boolean getStepCheck(boolean chek) {
        return StepCheked;
    }

    public void setTimeCheck(boolean chek) {
        timeCheked = chek;
    }

    public boolean getTimeChek() {
        return timeCheked;
    }

}
